<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Coalition Technologies - Anıl Özmen</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>

<div class="container p-5">
    <div class="row">
        <div class="col">
            <form method="POST">
                @csrf
                @method('POST')

                <div class="form-group row text-center" id="messages"></div>
                <div class="form-group row">
                    <div class="col-12">
                        <label for="product_name" class="text-right">Product Name</label>
                        <input type="text" class="form-control" id="product_name" name="product_name"
                               placeholder="Product Name"
                               value="{{ old('product_name') }}" required>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12">
                        <label for="quantity" class="text-right">Quantity</label>
                        <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity"
                               value="{{ old('quantity') }}" required>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12">
                        <label for="price" class="text-right">Price</label>
                        <input type="text" class="form-control" id="price" name="price" placeholder="Price"
                               value="{{ old('price') }}" required>
                    </div>
                </div>

                <h2>


                </h2>

                <div class="row pt-2 pt-sm-5 mt-1">
                    <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">

                    </div>
                    <div class="col-sm-6 pl-0">
                        <p class="text-right">
                            <button type="submit" id="save-product" class="btn btn-space btn-primary">Save</button>
                        </p>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h3 id="totalValues"></h3>
            <br><br>
        </div>
    </div>
    <div class="row" id="products">
    </div>

</div>


<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script>
    $(document).ready(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });



        $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: '{{ route('datas') }}',
            success: function (data) {
                $.each(data.datas, function (index, item) {
                    let content = $('#products');
                    console.log(index, item);
                    let productName = item.product_name;
                    let productQTY = item.quantity;
                    let dateTime = item.datetime;
                    let price = item.price;
                    let totalVN = item.total_value_number;

                    content.append(`<div class="col-md-4"><h1>Product Name: ${productName}</h1><p>Quantity: ${productQTY}</p><p>Price: ${price}</p><p>Total Value Number: ${totalVN}</p><p>Created At: ${dateTime}</p></div>`);
                })

                let allVNo = data.total_val_nu;
                let totalValDiv = $("#totalValues")[0];
                totalValDiv.textContent = `All of the Total Value numbers: ${parseFloat(allVNo).toFixed(2)}`;

            },
            error: function () {
                console.log(data);
            },
        });


        $('#save-product').click(function (e) {
            e.preventDefault();
            let product_name = $('#product_name').val();
            let quantity = $('#quantity').val();
            let price = $('#price').val();
            let totalValueNumber = quantity * price;
            let datetime = '{{ \Illuminate\Support\Carbon::now() }}';

            $.ajax({
                method: 'POST',
                url: '{{ route('products.store') }}',
                data: {
                    product_name: product_name,
                    quantity: quantity,
                    price: price,
                    datetime: datetime,
                    total_value_number: totalValueNumber
                },
                success: function (data) {
                    console.log(data.success);
                    let content = $('#products');
                    content.append(`<div class="col-md-4"><h1>Product Name: ${product_name}</h1><p>Quantity: ${quantity}</p><p>Price: ${price}</p><p>Total Value Number: ${totalValueNumber}</p><p>Created At: ${datetime}</p></div>`);


                    $('#messages')[0].textContent = data.success;
                    setTimeout(function () {
                        $('#messages')[0].style.display = "none";
                    },3000);
                },
                error: function (e) {
                    console.log(e.error);
                    $('#messages')[0].textContent = e.error;
                    setTimeout(function () {
                        $('#messages')[0].style.display = "none";
                    },3000);
                }
            });
        });


    });
</script>
</body>
</html>